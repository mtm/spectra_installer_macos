import argparse
import hashlib
import logging
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile
import zipfile

import bs4
import furl
import humanize
import magic
import pkg_resources
import requests

import mylogger


def add_parser(subparsers):
    parser = subparsers.add_parser(
        "push-pkg",
        help="fetch and upload macOS pkg",
        aliases=["pkg"],
    )
    add_arguments(parser)


def add_arguments(parser):
    parser.add_argument("-d", "--debug", default=False, action="store_true")
    parser.add_argument("-ff", "--force-fetch", default=False, action="store_true")
    parser.add_argument(
        "-dr",
        "--dry-run",
        default=False,
        action="store_true",
        help="do all the steps as normal but when you're syncing to s3 bucket, don't actually sync but do dry run",
    )


def filelist_recurse(directory: pathlib.Path):
    return [
        pathlib.Path(dp) / f
        for dp, dn, fn in os.walk(pathlib.Path.expanduser(directory))
        for f in fn
    ]


def main(args):
    logger = logging.getLogger(__name__)

    source = furl.furl(
        "https://www.dropbox.com/s/e1w4awt04gte9qo/Streambox%20Spectra%20for%20macOS.1.7.9.pkg"
    )
    source.args["dl"] = "1"

    public = furl.furl(
        "https://streambox-spectra.s3-us-west-2.amazonaws.com/latest/macos/spectra.zip"
    )
    push = furl.furl("s3://streambox-spectra/latest/macos/spectra.zip")

    base = pathlib.Path("scratch")
    step1 = base / "step1"
    step2 = base / "step2"
    step3 = base / "step3"
    step4 = base / "step4"
    step5 = base / "step5" / "test"
    step6 = base / "step6" / "latest/macos"

    shutil.rmtree(step2, ignore_errors=True)
    shutil.rmtree(step3, ignore_errors=True)
    shutil.rmtree(step4, ignore_errors=True)

    pathlib.Path.mkdir(step1, parents=True, exist_ok=True)
    pathlib.Path.mkdir(step2, parents=True, exist_ok=True)
    # pathlib.Path.mkdir(step3, parents=True, exist_ok=True)
    pathlib.Path.mkdir(step4, parents=True, exist_ok=True)
    pathlib.Path.mkdir(step5, parents=True, exist_ok=True)
    pathlib.Path.mkdir(step6, parents=True, exist_ok=True)

    m = hashlib.sha256()

    m.update(source.url.encode())
    digest = m.hexdigest()

    package = step1 / source.path.segments[-1]

    if not package.exists() or args.force_fetch:
        tmp_path = pathlib.Path(tempfile.gettempdir()) / digest
        logger.debug(
            f"fetching {source.url} to {tmp_path} and moving to '{package.absolute()}'"
        )
        r = requests.get(source.url, allow_redirects=True)
        tmp_path.unlink(missing_ok=True)
        open(tmp_path, "wb").write(r.content)
        logger.debug(f"renaming {tmp_path.absolute()} to {package.absolute()}")
        tmp_path.rename(f"{package.absolute()}")

    if not package.exists():
        logger.exception(f"I can't continue unless {package} exists.  Giving up.")
        sys.exit(-1)

    package = package.resolve()

    file_type = magic.from_file(str(package), mime=True)

    logger.debug(f"{package.name} has file type {file_type}")
    logger.debug(
        f"{package.name} has file size {humanize.naturalsize(package.stat().st_size)}"
    )

    if not file_type == "application/x-xar":
        logger.exception(
            f"{package} is not in expected format.  I'm expecting application/x-xar.  Giving up."
        )
        sys.exit(-1)

    cmd = [
        "pkgutil",
        "--expand-full",
        package.resolve(),
        step3.absolute(),
    ]
    logger.debug(" ".join([str(part) for part in cmd]))

    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    stdout, stderr = process.communicate()

    if stderr:
        logger.warning("{}".format(stderr.decode()))
        raise ValueError(stderr.decode())

    glob_gen = step3 / "**"

    fl1 = filelist_recurse(step3)

    _str = " ".join([str(item) for item in fl1])
    # logger.debug(f"file list: {_str}")

    distribution = step3 / "Distribution"

    if not distribution.exists():
        logger.exception(
            f"I can't find the version without having a {distribution}.  Giving up."
        )
        sys.exit(-1)

    soup = bs4.BeautifulSoup(distribution.read_text(), "lxml")
    elem = soup.find("pkg-ref", version=True)

    if not elem:
        logger.exception(
            f"I can't find 'version' attribute from {distrubtion}.  Giving up."
        )
        sys.exit(-1)

    version = elem["version"]
    logger.debug(
        (
            f"{version=} is parsed as {pkg_resources.parse_version(version)} by "
            "pkg_resources.parse_version(version)"
        )
    )

    if version != str(pkg_resources.parse_version(version)):
        logger.debug(
            (
                f"I see that {version=} is different from the parsed version from "
                "pkg_resources.parse_version(version), but that is ok. I'm only using "
                "pkg_resources.parse_version(version) to determine if the version "
                "number is unparseable."
            )
        )
    logger.debug("from {} I see version is {}".format(distribution, version))

    zip_path = step4 / "spectra.zip"
    logger.debug(f"creating archive {zip_path}")

    glob_gen = step3 / "**"

    zf = zipfile.ZipFile(str(zip_path), mode="w")

    orig = os.getcwd()
    try:
        os.chdir(package.resolve().parent)
        zf.write(package.name, compress_type=zipfile.ZIP_DEFLATED)
    except BaseException as ex:
        logger.exception(ex)
    finally:
        logger.debug(f"closing zip file {zip_path}")
        zf.close()
        os.chdir(orig)

    # test extracting the zip
    with zipfile.ZipFile(zip_path, "r") as zip_ref:
        zip_ref.extractall(step5)

    fl1 = filelist_recurse(step5)
    logger.debug(" ".join([str(item) for item in fl1]))

    resources_path = (
        step3
        / "SpectraMacOSInstaller.pkg/Payload/SpectraControlPanel.app/Contents/Resources"
    )
    encassist_ini = resources_path / "encassist.ini"
    version_path = resources_path / "version.txt"
    version_txt = version_path.read_text().strip()

    logger.debug(f"encassist.ini:\n{encassist_ini.read_text()}")

    assert version_txt == version
    assert len(list(step5.glob("*.pkg"))) == 1
    assert version

    # staging
    vfile = step6 / "version.txt"
    vfile.write_text(version)
    shutil.copy(zip_path, step6)

    logger.debug(
        (
            f"{version=} is parsed as {pkg_resources.parse_version(version)} by "
            "pkg_resources.parse_version(version)"
        )
    )

    # last check
    fl1 = filelist_recurse(step6)
    logger.debug(" ".join([str(item) for item in fl1]))

    version_obj = pkg_resources.parse_version(vfile.read_text())
    parsed = f"{version_obj}"

    logger.debug(f"re-reading {vfile} to verify version: {parsed}")

    if not parsed:
        logger.exception(
            (
                f"I write version to {vfile}, but when I read it "
                f"back, it doesn't parse.  {vfile} parses as {parsed}.  Giving up."
            )
        )
        sys.exit(-1)

    sync = [
        "aws",
        "s3",
        "sync",
        str(step6.resolve()),
        "s3://streambox-spectra/latest/macos",
        "--grants",
        "read=uri=http://acs.amazonaws.com/groups/global/AllUsers",
        "--no-progress",
        "--region",
        "us-west-2",
    ]

    if args.dry_run:
        sync.extend("--dryrun")

    logger.debug(" ".join(sync))

    process = subprocess.Popen(
        sync,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    stdout, stderr = process.communicate()
    logger.debug("\n{}".format(stdout.decode()))

    if stderr:
        logger.warning("{}".format(stderr.decode()))
        raise ValueError(stderr.decode())

    # verify version is as expected and endpoint is readable
    if not args.dry_run:
        url = "https://streambox-spectra.s3-us-west-2.amazonaws.com/latest/macos/version.txt"
        r = requests.get(url)
        assert r.content.decode() == version_txt


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()

    if sys.version_info < (3, 7):
        raise Exception("need at least python3.7")

    if args.debug:
        mylogger.stream.setLevel(logging.DEBUG)

    main(args)
